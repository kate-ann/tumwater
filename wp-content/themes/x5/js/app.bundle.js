/******/ (function(modules) { // webpackBootstrap
/******/  // The module cache
/******/  var installedModules = {};
/******/
/******/  // The require function
/******/  function __webpack_require__(moduleId) {
/******/
/******/    // Check if module is in cache
/******/    if(installedModules[moduleId]) {
/******/      return installedModules[moduleId].exports;
/******/    }
/******/    // Create a new module (and put it into the cache)
/******/    var module = installedModules[moduleId] = {
/******/      i: moduleId,
/******/      l: false,
/******/      exports: {}
/******/    };
/******/
/******/    // Execute the module function
/******/    modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/    // Flag the module as loaded
/******/    module.l = true;
/******/
/******/    // Return the exports of the module
/******/    return module.exports;
/******/  }
/******/
/******/
/******/  // expose the modules object (__webpack_modules__)
/******/  __webpack_require__.m = modules;
/******/
/******/  // expose the module cache
/******/  __webpack_require__.c = installedModules;
/******/
/******/  // define getter function for harmony exports
/******/  __webpack_require__.d = function(exports, name, getter) {
/******/    if(!__webpack_require__.o(exports, name)) {
/******/      Object.defineProperty(exports, name, {
/******/        configurable: false,
/******/        enumerable: true,
/******/        get: getter
/******/      });
/******/    }
/******/  };
/******/
/******/  // getDefaultExport function for compatibility with non-harmony modules
/******/  __webpack_require__.n = function(module) {
/******/    var getter = module && module.__esModule ?
/******/      function getDefault() { return module['default']; } :
/******/      function getModuleExports() { return module; };
/******/    __webpack_require__.d(getter, 'a', getter);
/******/    return getter;
/******/  };
/******/
/******/  // Object.prototype.hasOwnProperty.call
/******/  __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/  // __webpack_public_path__
/******/  __webpack_require__.p = "";
/******/
/******/  // Load entry module and return exports
/******/  return __webpack_require__(__webpack_require__.s = "multi /src/scripts/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "0iPh":
/***/ (function(module, exports) {

module.exports = window.jQuery;

/***/ }),

/***/ "A2qv":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = __webpack_require__("0iPh");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Add background videos to sections
 */
var initVideos = function initVideos() {

  var introVideoBg = new video_background((0, _jquery2.default)('#js-intro-player'), {
    'position': 'fixed',
    'z-index': '-1',

    'loop': true, //Loop when it reaches the end
    'autoplay': true,
    'muted': true,

    'mp4': '',
    'webm': '',
    'youtube': x5_video.video_yt,
    'video_ratio': 1.7778, // width/height -> If none provided sizing of the video is set to adjust

    'fallback_image': '' //Fallback image path
  });
};

exports.default = initVideos;

/***/ }),

/***/ "C/RU":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = __webpack_require__("0iPh");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Add smooth scroll to all buttons
 */
var smoothScrollToSections = function smoothScrollToSections() {

  (0, _jquery2.default)('a[href*="#"]:not([href="#"])').not('.comment-reply-link').click(function () {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

      var linkHash = (0, _jquery2.default)(this).attr('href');
      var target = (0, _jquery2.default)(this.hash);
      target = target.length ? target : (0, _jquery2.default)('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        (0, _jquery2.default)('html, body').animate({
          scrollTop: target.offset().top
        }, {
          duration: 1000,
          complete: function complete() {
            location.href = linkHash;
          }
        });
        return false;
      }
    }
    return null;
  });
};

exports.default = smoothScrollToSections;

/***/ }),

/***/ "JVLD":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = __webpack_require__("0iPh");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Init page navigation
 */
var pageNav = function pageNav() {
  // Cache selectors
  var lastId = '';
  var topMenu = (0, _jquery2.default)('.js-page-nav');
  var topMenuHeight = topMenu.outerHeight() + 1;

  // All list items
  var menuItems = topMenu.find('a');

  // Anchors corresponding to menu items
  var scrollItems = menuItems.map(function () {
    var item = (0, _jquery2.default)((0, _jquery2.default)(this).attr('href'));
    if (item.length) {
      return item;
    }
  });

  var menuLinks = (0, _jquery2.default)('.js-page-nav a');

  menuLinks.each(function () {
    (0, _jquery2.default)(this).click(function () {
      if (!(0, _jquery2.default)(this).parent().hasClass('active')) {
        (0, _jquery2.default)('.js-page-nav .active').removeClass('active');
        (0, _jquery2.default)(this).parent().addClass('active');
      }
    });
  });

  // Bind to scroll
  (0, _jquery2.default)(window).scroll(function () {

    // Get container scroll position
    var fromTop = (0, _jquery2.default)(this).scrollTop() + topMenuHeight;

    // Remove current scroll item
    var cur = scrollItems.map(function () {
      if ((0, _jquery2.default)(this).offset().top < fromTop) {
        return this;
      }
    });

    // Remove id of the current element
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : '';

    if (lastId !== id) {
      lastId = id;

      // Set/remove active class
      menuItems.parent().removeClass('active');
      (0, _jquery2.default)('.js-page-nav a[href="#' + id + '"]').parent().addClass('active');
    }
  });
};

exports.default = pageNav;

/***/ }),

/***/ "g7Pl":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _smoothScrollToSections = __webpack_require__("C/RU");

var _smoothScrollToSections2 = _interopRequireDefault(_smoothScrollToSections);

var _menu = __webpack_require__("lmGI");

var _menu2 = _interopRequireDefault(_menu);

var _pageNav = __webpack_require__("JVLD");

var _pageNav2 = _interopRequireDefault(_pageNav);

var _videoBackground = __webpack_require__("A2qv");

var _videoBackground2 = _interopRequireDefault(_videoBackground);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
  Project: Tumwater
  Author: Xfive
 */

(0, _menu2.default)();
(0, _smoothScrollToSections2.default)();
(0, _pageNav2.default)();
(0, _videoBackground2.default)();

/***/ }),

/***/ "lmGI":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = __webpack_require__("0iPh");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Init menu
 */
var menu = function menu() {

  // hide menu
  (0, _jquery2.default)('.sub-menu, .js-nav__menu').slideUp({ duration: 0 });

  // init submenu
  (0, _jquery2.default)('.menu-item-has-children').append('<div class="c-nav__open-menu-btn js-nav__open-menu-btn"></div>');

  // handle menu btn click
  var isOpen = false;

  (0, _jquery2.default)('.js-nav__btn').click(function () {

    if (isOpen) {
      (0, _jquery2.default)('.sub-menu, .js-nav__menu').slideUp();
      (0, _jquery2.default)('.sub-menu').removeClass('toggled');
      isOpen = false;
    } else {
      (0, _jquery2.default)('.js-nav__menu').slideDown();
      isOpen = true;
    }
  });

  // handle submenu btn click
  (0, _jquery2.default)('.js-nav__open-menu-btn').click(function () {
    var submenu = (0, _jquery2.default)(this).parent().find('.sub-menu');

    if ((0, _jquery2.default)(submenu).hasClass('toggled')) {
      (0, _jquery2.default)(submenu).slideUp({ duration: 300 }).removeClass('toggled');
      (0, _jquery2.default)(this).removeClass('close');
    } else {
      (0, _jquery2.default)(submenu).slideDown({ duration: 300 }).addClass('toggled');
      (0, _jquery2.default)(this).addClass('close');
    }
  });

  // handle click outside menu
  (0, _jquery2.default)('.js-nav').focusout(function () {
    if (isOpen) {
      (0, _jquery2.default)('.js-nav__menu').slideUp();
      isOpen = false;
    }
  });
};

exports.default = menu;

/***/ }),

/***/ "multi /src/scripts/app.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("g7Pl");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjU4MmVlNWJhZTkxNzIyODhiYmUiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwid2luZG93LmpRdWVyeVwiIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL21vZHVsZXMvdmlkZW9CYWNrZ3JvdW5kLmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL21vZHVsZXMvc21vb3RoU2Nyb2xsVG9TZWN0aW9ucy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9tb2R1bGVzL3BhZ2UtbmF2LmpzIiwid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9tb2R1bGVzL21lbnUuanMiXSwibmFtZXMiOlsiaW5pdFZpZGVvcyIsImludHJvVmlkZW9CZyIsInZpZGVvX2JhY2tncm91bmQiLCJzbW9vdGhTY3JvbGxUb1NlY3Rpb25zIiwibm90IiwiY2xpY2siLCJsb2NhdGlvbiIsInBhdGhuYW1lIiwicmVwbGFjZSIsImhvc3RuYW1lIiwibGlua0hhc2giLCJhdHRyIiwidGFyZ2V0IiwiaGFzaCIsImxlbmd0aCIsInNsaWNlIiwiYW5pbWF0ZSIsInNjcm9sbFRvcCIsIm9mZnNldCIsInRvcCIsImR1cmF0aW9uIiwiY29tcGxldGUiLCJocmVmIiwicGFnZU5hdiIsImxhc3RJZCIsInRvcE1lbnUiLCJ0b3BNZW51SGVpZ2h0Iiwib3V0ZXJIZWlnaHQiLCJtZW51SXRlbXMiLCJmaW5kIiwic2Nyb2xsSXRlbXMiLCJtYXAiLCJpdGVtIiwibWVudUxpbmtzIiwiZWFjaCIsInBhcmVudCIsImhhc0NsYXNzIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsIndpbmRvdyIsInNjcm9sbCIsImZyb21Ub3AiLCJjdXIiLCJpZCIsIm1lbnUiLCJzbGlkZVVwIiwiYXBwZW5kIiwiaXNPcGVuIiwic2xpZGVEb3duIiwic3VibWVudSIsImZvY3Vzb3V0Il0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDN0RBLCtCOzs7Ozs7Ozs7Ozs7OztBQ0FBOzs7Ozs7QUFFQTs7O0FBR0EsSUFBTUEsYUFBYSxTQUFiQSxVQUFhLEdBQU07O0FBRXZCLE1BQU1DLGVBQWUsSUFBSUMsZ0JBQUosQ0FBcUIsc0JBQUUsa0JBQUYsQ0FBckIsRUFBNEM7QUFDL0QsZ0JBQVksVUFEbUQsRUFDdkM7QUFDeEIsZUFBVyxJQUZvRCxFQUV2Qzs7QUFFeEIsWUFBUSxJQUp1RCxFQUl2QztBQUN4QixnQkFBWSxJQUxtRCxFQUt2QztBQUN4QixhQUFTLElBTnNELEVBTXZDOztBQUV4QixXQUFPLEVBUndELEVBUWxEO0FBQ2IsWUFBUSxFQVR1RCxFQVMvQztBQUNoQixlQUFXLGFBVm9EO0FBVy9ELG1CQUFlLE1BWGdELEVBV25DOztBQUU1QixzQkFBa0IsRUFiNkMsQ0FhdEM7QUFic0MsR0FBNUMsQ0FBckI7QUFlRCxDQWpCRDs7a0JBbUJlRixVOzs7Ozs7Ozs7Ozs7OztBQ3hCZjs7Ozs7O0FBRUE7OztBQUdBLElBQU1HLHlCQUF5QixTQUF6QkEsc0JBQXlCLEdBQU07O0FBRW5DLHdCQUFFLDhCQUFGLEVBQWtDQyxHQUFsQyxDQUFzQyxxQkFBdEMsRUFBNkRDLEtBQTdELENBQW1FLFlBQVc7QUFDNUUsUUFBSUMsU0FBU0MsUUFBVCxDQUFrQkMsT0FBbEIsQ0FBMEIsS0FBMUIsRUFBaUMsRUFBakMsTUFBeUMsS0FBS0QsUUFBTCxDQUFjQyxPQUFkLENBQXNCLEtBQXRCLEVBQTZCLEVBQTdCLENBQXpDLElBQ0ZGLFNBQVNHLFFBQVQsS0FBc0IsS0FBS0EsUUFEN0IsRUFDdUM7O0FBRXJDLFVBQU1DLFdBQVcsc0JBQUUsSUFBRixFQUFRQyxJQUFSLENBQWEsTUFBYixDQUFqQjtBQUNBLFVBQUlDLFNBQVMsc0JBQUUsS0FBS0MsSUFBUCxDQUFiO0FBQ0FELGVBQVNBLE9BQU9FLE1BQVAsR0FBZ0JGLE1BQWhCLEdBQXlCLHNCQUFFLFdBQVcsS0FBS0MsSUFBTCxDQUFVRSxLQUFWLENBQWdCLENBQWhCLENBQVgsR0FBZ0MsR0FBbEMsQ0FBbEM7QUFDQSxVQUFJSCxPQUFPRSxNQUFYLEVBQW1CO0FBQ2pCLDhCQUFFLFlBQUYsRUFBZ0JFLE9BQWhCLENBQXdCO0FBQ3RCQyxxQkFBV0wsT0FBT00sTUFBUCxHQUFnQkM7QUFETCxTQUF4QixFQUVHO0FBQ0RDLG9CQUFVLElBRFQ7QUFFREMsb0JBQVUsb0JBQVc7QUFDbkJmLHFCQUFTZ0IsSUFBVCxHQUFnQlosUUFBaEI7QUFDRDtBQUpBLFNBRkg7QUFRQSxlQUFPLEtBQVA7QUFDRDtBQUNGO0FBQ0QsV0FBTyxJQUFQO0FBQ0QsR0FwQkQ7QUFxQkQsQ0F2QkQ7O2tCQXlCZVAsc0I7Ozs7Ozs7Ozs7Ozs7O0FDOUJmOzs7Ozs7QUFFQTs7O0FBR0EsSUFBTW9CLFVBQVUsU0FBVkEsT0FBVSxHQUFNO0FBQ3BCO0FBQ0EsTUFBSUMsU0FBUyxFQUFiO0FBQ0EsTUFBTUMsVUFBVSxzQkFBRSxjQUFGLENBQWhCO0FBQ0EsTUFBSUMsZ0JBQWdCRCxRQUFRRSxXQUFSLEtBQXNCLENBQTFDOztBQUVBO0FBQ0EsTUFBTUMsWUFBWUgsUUFBUUksSUFBUixDQUFhLEdBQWIsQ0FBbEI7O0FBRUE7QUFDQSxNQUFNQyxjQUFjRixVQUFVRyxHQUFWLENBQWMsWUFBVTtBQUMxQyxRQUFJQyxPQUFPLHNCQUFFLHNCQUFFLElBQUYsRUFBUXJCLElBQVIsQ0FBYSxNQUFiLENBQUYsQ0FBWDtBQUNBLFFBQUlxQixLQUFLbEIsTUFBVCxFQUFpQjtBQUNmLGFBQU9rQixJQUFQO0FBQ0Q7QUFDRixHQUxtQixDQUFwQjs7QUFPQSxNQUFNQyxZQUFZLHNCQUFFLGdCQUFGLENBQWxCOztBQUVBQSxZQUFVQyxJQUFWLENBQWUsWUFBVTtBQUN2QiwwQkFBRSxJQUFGLEVBQVE3QixLQUFSLENBQWMsWUFBVTtBQUN0QixVQUFHLENBQUMsc0JBQUUsSUFBRixFQUFROEIsTUFBUixHQUFpQkMsUUFBakIsQ0FBMEIsUUFBMUIsQ0FBSixFQUF5QztBQUN2Qyw4QkFBRSxzQkFBRixFQUEwQkMsV0FBMUIsQ0FBc0MsUUFBdEM7QUFDQSw4QkFBRSxJQUFGLEVBQVFGLE1BQVIsR0FBaUJHLFFBQWpCLENBQTBCLFFBQTFCO0FBQ0Q7QUFDRixLQUxEO0FBTUQsR0FQRDs7QUFVQTtBQUNBLHdCQUFFQyxNQUFGLEVBQVVDLE1BQVYsQ0FBaUIsWUFBVTs7QUFFekI7QUFDQSxRQUFJQyxVQUFVLHNCQUFFLElBQUYsRUFBUXhCLFNBQVIsS0FBb0JTLGFBQWxDOztBQUVBO0FBQ0EsUUFBSWdCLE1BQU1aLFlBQVlDLEdBQVosQ0FBZ0IsWUFBVTtBQUNsQyxVQUFJLHNCQUFFLElBQUYsRUFBUWIsTUFBUixHQUFpQkMsR0FBakIsR0FBdUJzQixPQUEzQixFQUFtQztBQUNqQyxlQUFPLElBQVA7QUFDRDtBQUNGLEtBSlMsQ0FBVjs7QUFNQTtBQUNBQyxVQUFNQSxJQUFJQSxJQUFJNUIsTUFBSixHQUFXLENBQWYsQ0FBTjtBQUNBLFFBQUk2QixLQUFLRCxPQUFPQSxJQUFJNUIsTUFBWCxHQUFvQjRCLElBQUksQ0FBSixFQUFPQyxFQUEzQixHQUFnQyxFQUF6Qzs7QUFFQSxRQUFJbkIsV0FBV21CLEVBQWYsRUFBbUI7QUFDakJuQixlQUFTbUIsRUFBVDs7QUFFQTtBQUNBZixnQkFBVU8sTUFBVixHQUFtQkUsV0FBbkIsQ0FBK0IsUUFBL0I7QUFDQSw0QkFBRSwyQkFBeUJNLEVBQXpCLEdBQTRCLElBQTlCLEVBQW9DUixNQUFwQyxHQUE2Q0csUUFBN0MsQ0FBc0QsUUFBdEQ7QUFDRDtBQUNGLEdBdkJEO0FBd0JELENBdEREOztrQkF3RGVmLE87Ozs7Ozs7Ozs7QUN4RGY7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQVJBOzs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBLGlDOzs7Ozs7Ozs7Ozs7OztBQ2JBOzs7Ozs7QUFFQTs7O0FBR0EsSUFBTXFCLE9BQU8sU0FBUEEsSUFBTyxHQUFNOztBQUVqQjtBQUNBLHdCQUFFLDBCQUFGLEVBQThCQyxPQUE5QixDQUFzQyxFQUFFekIsVUFBVSxDQUFaLEVBQXRDOztBQUVBO0FBQ0Esd0JBQUUseUJBQUYsRUFBNkIwQixNQUE3QixDQUNFLGdFQURGOztBQUlBO0FBQ0EsTUFBSUMsU0FBUyxLQUFiOztBQUVBLHdCQUFFLGNBQUYsRUFBa0IxQyxLQUFsQixDQUF3QixZQUFXOztBQUVqQyxRQUFJMEMsTUFBSixFQUFZO0FBQ1YsNEJBQUUsMEJBQUYsRUFBOEJGLE9BQTlCO0FBQ0EsNEJBQUUsV0FBRixFQUFlUixXQUFmLENBQTJCLFNBQTNCO0FBQ0FVLGVBQVMsS0FBVDtBQUNELEtBSkQsTUFJTztBQUNMLDRCQUFFLGVBQUYsRUFBbUJDLFNBQW5CO0FBQ0FELGVBQVMsSUFBVDtBQUNEO0FBQ0YsR0FWRDs7QUFZQTtBQUNBLHdCQUFFLHdCQUFGLEVBQTRCMUMsS0FBNUIsQ0FBa0MsWUFBVztBQUMzQyxRQUFNNEMsVUFBVSxzQkFBRSxJQUFGLEVBQ2JkLE1BRGEsR0FFYk4sSUFGYSxDQUVSLFdBRlEsQ0FBaEI7O0FBSUEsUUFBSSxzQkFBRW9CLE9BQUYsRUFBV2IsUUFBWCxDQUFvQixTQUFwQixDQUFKLEVBQW9DO0FBQ2xDLDRCQUFFYSxPQUFGLEVBQVdKLE9BQVgsQ0FBbUIsRUFBRXpCLFVBQVUsR0FBWixFQUFuQixFQUFzQ2lCLFdBQXRDLENBQWtELFNBQWxEO0FBQ0EsNEJBQUUsSUFBRixFQUFRQSxXQUFSLENBQW9CLE9BQXBCO0FBQ0QsS0FIRCxNQUdPO0FBQ0wsNEJBQUVZLE9BQUYsRUFBV0QsU0FBWCxDQUFxQixFQUFFNUIsVUFBVSxHQUFaLEVBQXJCLEVBQXdDa0IsUUFBeEMsQ0FBaUQsU0FBakQ7QUFDQSw0QkFBRSxJQUFGLEVBQVFBLFFBQVIsQ0FBaUIsT0FBakI7QUFDRDtBQUNGLEdBWkQ7O0FBY0E7QUFDQSx3QkFBRSxTQUFGLEVBQWFZLFFBQWIsQ0FBc0IsWUFBVztBQUMvQixRQUFJSCxNQUFKLEVBQVk7QUFDViw0QkFBRSxlQUFGLEVBQW1CRixPQUFuQjtBQUNBRSxlQUFTLEtBQVQ7QUFDRDtBQUNGLEdBTEQ7QUFNRCxDQS9DRDs7a0JBaURlSCxJIiwiZmlsZSI6ImFwcC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIm11bHRpIC9zcmMvc2NyaXB0cy9hcHAuanNcIik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjU4MmVlNWJhZTkxNzIyODhiYmUiLCJtb2R1bGUuZXhwb3J0cyA9IHdpbmRvdy5qUXVlcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gZXh0ZXJuYWwgXCJ3aW5kb3cualF1ZXJ5XCJcbi8vIG1vZHVsZSBpZCA9IDBpUGhcbi8vIG1vZHVsZSBjaHVua3MgPSBhcHAiLCJpbXBvcnQgJCBmcm9tICdqcXVlcnknO1xuXG4vKlxuICogQWRkIGJhY2tncm91bmQgdmlkZW9zIHRvIHNlY3Rpb25zXG4gKi9cbmNvbnN0IGluaXRWaWRlb3MgPSAoKSA9PiB7XG5cbiAgY29uc3QgaW50cm9WaWRlb0JnID0gbmV3IHZpZGVvX2JhY2tncm91bmQoJCgnI2pzLWludHJvLXBsYXllcicpLCB7XG4gICAgJ3Bvc2l0aW9uJzogJ2Fic29sdXRlJywgLy9TdGljayB3aXRoaW4gdGhlIGRpdlxuICAgICd6LWluZGV4JzogJy0xJywgICAgICAgIC8vQmVoaW5kIGV2ZXJ5dGhpbmdcblxuICAgICdsb29wJzogdHJ1ZSwgICAgICAgICAgIC8vTG9vcCB3aGVuIGl0IHJlYWNoZXMgdGhlIGVuZFxuICAgICdhdXRvcGxheSc6IHRydWUsICAgICAgIC8vQXV0b3BsYXkgYXQgc3RhcnRcbiAgICAnbXV0ZWQnOiB0cnVlLCAgICAgICAgICAvL011dGVkIGF0IHN0YXJ0XG5cbiAgICAnbXA0JzogJycgLCAgLy9QYXRoIHRvIHZpZGVvIG1wNCBmb3JtYXRcbiAgICAnd2VibSc6ICcnICwgICAgLy9QYXRoIHRvIHZpZGVvIHdlYm0gZm9ybWF0XG4gICAgJ3lvdXR1YmUnOiAnQnhtWXZWVGExOW8nLFxuICAgICd2aWRlb19yYXRpbyc6IDEuNzc3OCwgICAgICAvLyB3aWR0aC9oZWlnaHQgLT4gSWYgbm9uZSBwcm92aWRlZCBzaXppbmcgb2YgdGhlIHZpZGVvIGlzIHNldCB0byBhZGp1c3RcblxuICAgICdmYWxsYmFja19pbWFnZSc6ICcnLCAgICAvL0ZhbGxiYWNrIGltYWdlIHBhdGhcbiAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBpbml0VmlkZW9zO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3NjcmlwdHMvbW9kdWxlcy92aWRlb0JhY2tncm91bmQuanMiLCJpbXBvcnQgJCBmcm9tICdqcXVlcnknO1xuXG4vKlxuICogQWRkIHNtb290aCBzY3JvbGwgdG8gYWxsIGJ1dHRvbnNcbiAqL1xuY29uc3Qgc21vb3RoU2Nyb2xsVG9TZWN0aW9ucyA9ICgpID0+IHtcblxuICAkKCdhW2hyZWYqPVwiI1wiXTpub3QoW2hyZWY9XCIjXCJdKScpLm5vdCgnLmNvbW1lbnQtcmVwbHktbGluaycpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgIGlmIChsb2NhdGlvbi5wYXRobmFtZS5yZXBsYWNlKC9eXFwvLywgJycpID09PSB0aGlzLnBhdGhuYW1lLnJlcGxhY2UoL15cXC8vLCAnJykgJiZcbiAgICAgIGxvY2F0aW9uLmhvc3RuYW1lID09PSB0aGlzLmhvc3RuYW1lKSB7XG5cbiAgICAgIGNvbnN0IGxpbmtIYXNoID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG4gICAgICBsZXQgdGFyZ2V0ID0gJCh0aGlzLmhhc2gpO1xuICAgICAgdGFyZ2V0ID0gdGFyZ2V0Lmxlbmd0aCA/IHRhcmdldCA6ICQoJ1tuYW1lPScgKyB0aGlzLmhhc2guc2xpY2UoMSkgKyAnXScpO1xuICAgICAgaWYgKHRhcmdldC5sZW5ndGgpIHtcbiAgICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICAgIHNjcm9sbFRvcDogdGFyZ2V0Lm9mZnNldCgpLnRvcFxuICAgICAgICB9LCB7XG4gICAgICAgICAgZHVyYXRpb246IDEwMDAsXG4gICAgICAgICAgY29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbG9jYXRpb24uaHJlZiA9IGxpbmtIYXNoO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgc21vb3RoU2Nyb2xsVG9TZWN0aW9ucztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9zY3JpcHRzL21vZHVsZXMvc21vb3RoU2Nyb2xsVG9TZWN0aW9ucy5qcyIsImltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XG5cbi8qXG4gKiBJbml0IHBhZ2UgbmF2aWdhdGlvblxuICovXG5jb25zdCBwYWdlTmF2ID0gKCkgPT4ge1xuICAvLyBDYWNoZSBzZWxlY3RvcnNcbiAgbGV0IGxhc3RJZCA9ICcnO1xuICBjb25zdCB0b3BNZW51ID0gJCgnLmpzLXBhZ2UtbmF2Jyk7XG4gIGxldCB0b3BNZW51SGVpZ2h0ID0gdG9wTWVudS5vdXRlckhlaWdodCgpKzE7XG5cbiAgLy8gQWxsIGxpc3QgaXRlbXNcbiAgY29uc3QgbWVudUl0ZW1zID0gdG9wTWVudS5maW5kKCdhJyk7XG5cbiAgLy8gQW5jaG9ycyBjb3JyZXNwb25kaW5nIHRvIG1lbnUgaXRlbXNcbiAgY29uc3Qgc2Nyb2xsSXRlbXMgPSBtZW51SXRlbXMubWFwKGZ1bmN0aW9uKCl7XG4gICAgdmFyIGl0ZW0gPSAkKCQodGhpcykuYXR0cignaHJlZicpKTtcbiAgICBpZiAoaXRlbS5sZW5ndGgpIHtcbiAgICAgIHJldHVybiBpdGVtO1xuICAgIH1cbiAgfSk7XG5cbiAgY29uc3QgbWVudUxpbmtzID0gJCgnLmpzLXBhZ2UtbmF2IGEnKTtcblxuICBtZW51TGlua3MuZWFjaChmdW5jdGlvbigpe1xuICAgICQodGhpcykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgIGlmKCEkKHRoaXMpLnBhcmVudCgpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xuICAgICAgICAkKCcuanMtcGFnZS1uYXYgLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJCh0aGlzKS5wYXJlbnQoKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuXG5cbiAgLy8gQmluZCB0byBzY3JvbGxcbiAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xuXG4gICAgLy8gR2V0IGNvbnRhaW5lciBzY3JvbGwgcG9zaXRpb25cbiAgICB2YXIgZnJvbVRvcCA9ICQodGhpcykuc2Nyb2xsVG9wKCkrdG9wTWVudUhlaWdodDtcblxuICAgIC8vIFJlbW92ZSBjdXJyZW50IHNjcm9sbCBpdGVtXG4gICAgdmFyIGN1ciA9IHNjcm9sbEl0ZW1zLm1hcChmdW5jdGlvbigpe1xuICAgICAgaWYgKCQodGhpcykub2Zmc2V0KCkudG9wIDwgZnJvbVRvcCl7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy8gUmVtb3ZlIGlkIG9mIHRoZSBjdXJyZW50IGVsZW1lbnRcbiAgICBjdXIgPSBjdXJbY3VyLmxlbmd0aC0xXTtcbiAgICB2YXIgaWQgPSBjdXIgJiYgY3VyLmxlbmd0aCA/IGN1clswXS5pZCA6ICcnO1xuXG4gICAgaWYgKGxhc3RJZCAhPT0gaWQpIHtcbiAgICAgIGxhc3RJZCA9IGlkO1xuXG4gICAgICAvLyBTZXQvcmVtb3ZlIGFjdGl2ZSBjbGFzc1xuICAgICAgbWVudUl0ZW1zLnBhcmVudCgpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICQoJy5qcy1wYWdlLW5hdiBhW2hyZWY9XCIjJytpZCsnXCJdJykucGFyZW50KCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBwYWdlTmF2O1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9zY3JpcHRzL21vZHVsZXMvcGFnZS1uYXYuanMiLCIvKlxuICBQcm9qZWN0OiBUdW13YXRlclxuICBBdXRob3I6IFhmaXZlXG4gKi9cblxuaW1wb3J0IHNtb290aFNjcm9sbFRvU2VjdGlvbnMgZnJvbSAnLi9tb2R1bGVzL3Ntb290aFNjcm9sbFRvU2VjdGlvbnMnO1xuaW1wb3J0IG1lbnUgZnJvbSAnLi9tb2R1bGVzL21lbnUnO1xuaW1wb3J0IHBhZ2VOYXYgZnJvbSAnLi9tb2R1bGVzL3BhZ2UtbmF2JztcbmltcG9ydCBpbml0VmlkZW9zIGZyb20gJy4vbW9kdWxlcy92aWRlb0JhY2tncm91bmQnO1xuXG5tZW51KCk7XG5zbW9vdGhTY3JvbGxUb1NlY3Rpb25zKCk7XG5wYWdlTmF2KCk7XG5pbml0VmlkZW9zKCk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3NjcmlwdHMvYXBwLmpzIiwiaW1wb3J0ICQgZnJvbSAnanF1ZXJ5JztcblxuLypcbiAqIEluaXQgbWVudVxuICovXG5jb25zdCBtZW51ID0gKCkgPT4ge1xuXG4gIC8vIGhpZGUgbWVudVxuICAkKCcuc3ViLW1lbnUsIC5qcy1uYXZfX21lbnUnKS5zbGlkZVVwKHsgZHVyYXRpb246IDAgfSk7XG5cbiAgLy8gaW5pdCBzdWJtZW51XG4gICQoJy5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJykuYXBwZW5kKFxuICAgICc8ZGl2IGNsYXNzPVwiYy1uYXZfX29wZW4tbWVudS1idG4ganMtbmF2X19vcGVuLW1lbnUtYnRuXCI+PC9kaXY+J1xuICApO1xuXG4gIC8vIGhhbmRsZSBtZW51IGJ0biBjbGlja1xuICBsZXQgaXNPcGVuID0gZmFsc2U7XG5cbiAgJCgnLmpzLW5hdl9fYnRuJykuY2xpY2soZnVuY3Rpb24oKSB7XG5cbiAgICBpZiAoaXNPcGVuKSB7XG4gICAgICAkKCcuc3ViLW1lbnUsIC5qcy1uYXZfX21lbnUnKS5zbGlkZVVwKCk7XG4gICAgICAkKCcuc3ViLW1lbnUnKS5yZW1vdmVDbGFzcygndG9nZ2xlZCcpO1xuICAgICAgaXNPcGVuID0gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoJy5qcy1uYXZfX21lbnUnKS5zbGlkZURvd24oKTtcbiAgICAgIGlzT3BlbiA9IHRydWU7XG4gICAgfVxuICB9KTtcblxuICAvLyBoYW5kbGUgc3VibWVudSBidG4gY2xpY2tcbiAgJCgnLmpzLW5hdl9fb3Blbi1tZW51LWJ0bicpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgIGNvbnN0IHN1Ym1lbnUgPSAkKHRoaXMpXG4gICAgICAucGFyZW50KClcbiAgICAgIC5maW5kKCcuc3ViLW1lbnUnKTtcblxuICAgIGlmICgkKHN1Ym1lbnUpLmhhc0NsYXNzKCd0b2dnbGVkJykpIHtcbiAgICAgICQoc3VibWVudSkuc2xpZGVVcCh7IGR1cmF0aW9uOiAzMDAgfSkucmVtb3ZlQ2xhc3MoJ3RvZ2dsZWQnKTtcbiAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2Nsb3NlJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoc3VibWVudSkuc2xpZGVEb3duKHsgZHVyYXRpb246IDMwMCB9KS5hZGRDbGFzcygndG9nZ2xlZCcpO1xuICAgICAgJCh0aGlzKS5hZGRDbGFzcygnY2xvc2UnKTtcbiAgICB9XG4gIH0pO1xuXG4gIC8vIGhhbmRsZSBjbGljayBvdXRzaWRlIG1lbnVcbiAgJCgnLmpzLW5hdicpLmZvY3Vzb3V0KGZ1bmN0aW9uKCkge1xuICAgIGlmIChpc09wZW4pIHtcbiAgICAgICQoJy5qcy1uYXZfX21lbnUnKS5zbGlkZVVwKCk7XG4gICAgICBpc09wZW4gPSBmYWxzZTtcbiAgICB9XG4gIH0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgbWVudTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvc2NyaXB0cy9tb2R1bGVzL21lbnUuanMiXSwic291cmNlUm9vdCI6IiJ9