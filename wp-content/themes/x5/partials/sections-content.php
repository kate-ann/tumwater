<?php if ( have_rows( 'x5_page_sections_rows' ) ):
	$x5_page_sections_row_class = 'active';
	$x5_page_sections_rows_align_class = '';
	$x5_page_sections_text_align_class = '';

	if ( get_field( 'x5_page_sections_menu_align' ) == 'right' ):
		$x5_page_sections_rows_align_class = 'right-align';
		$x5_page_sections_text_align_class = 'justify-content-end';
	endif;
	 ?>

	<nav class="page-nav js-page-nav <?php echo esc_attr( $x5_page_sections_rows_align_class ); ?>">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<ul>

						<?php while( have_rows( 'x5_page_sections_rows' ) ) : the_row(); ?>

							<?php if ( get_sub_field( 'x5_page_sections_rows_num' ) ||
												 get_sub_field( 'x5_page_sections_rows_title' ) ): ?>

								<li class="<?php echo esc_attr( $x5_page_sections_row_class ); ?>">
									<?php if ( get_sub_field( 'x5_page_sections_rows_id' ) ): ?>
										<a href="<?php echo esc_url( '#' . get_sub_field ( 'x5_page_sections_rows_id' ) ); ?>">

											<?php if ( get_sub_field( 'x5_page_sections_rows_num' ) ): ?>
												<span class="num"><?php echo esc_html( get_sub_field( 'x5_page_sections_rows_num' ) ); ?></span>
											<?php endif; ?>

											<?php if ( get_sub_field( 'x5_page_sections_rows_title' ) ): ?>
												<span class="link"><?php echo esc_html( get_sub_field( 'x5_page_sections_rows_title' ) ); ?></span>
											<?php endif; ?>

										</a>
									<?php endif; ?>

								</li>

							<?php endif; ?>

							<?php $x5_page_sections_row_class = ''; ?>

						<?php endwhile; ?>

					</ul>
				</div>
			</div>
		</div>
	</nav>
	<!-- page-nav -->

<?php endif; ?>


<?php if ( have_rows( 'x5_page_sections_rows' ) ): ?>

	<?php while( have_rows( 'x5_page_sections_rows' ) ) : the_row(); ?>

		<?php
		$x5_page_sections_rows_align_class = '';
		$x5_page_sections_text_align_class = '';

		if ( get_sub_field( 'x5_page_sections_rows_align' ) == 'right' ):
			$x5_page_sections_rows_align_class = 'right-align';
			$x5_page_sections_text_align_class = 'justify-content-end';
		endif;
		?>
		<?php if ( get_sub_field( 'x5_page_sections_rows_id' ) ): ?>
			<section id="<?php echo esc_attr( get_sub_field ( 'x5_page_sections_rows_id' ) ); ?>" class="c-content <?php echo esc_attr( get_sub_field ( 'x5_page_sections_rows_id' ) ); ?> <?php echo esc_attr( $x5_page_sections_rows_align_class ); ?>">
		<?php else: ?>
			<section class="c-content c-content-01 <?php echo esc_attr( $x5_page_sections_rows_align_class ); ?>">
		<?php endif; ?>

	  	<div class="container">

	  		<div class="row <?php echo esc_attr( $x5_page_sections_text_align_class ); ?>">
	  			<div class="col-lg-6 intro">

						<?php if ( get_sub_field( 'x5_page_sections_rows_desc' ) ): ?>
							<?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_page_sections_rows_desc' ) ) ); ?>
						<?php endif; ?>

	  			</div>
	  			<!-- col -->
	  		</div>
	  		<!-- row -->

	  	</div>
	  	<!-- container -->

	  </section>
	  <!-- c-content -->

	<?php endwhile; ?>

<?php endif; ?>