<?php
/**
 * X5: Theme specific functionalities
 *
 * Do not close any of the php files included with ?> closing tag!
 *
 * @package WordPress
 * @subpackage X5
 */

/*
 * Load X5 features
 */
function x5_load_features() {

	$features = scandir( dirname( __FILE__ ) . '/features/' );

	foreach ( $features as $feature ) {

		if ( current_theme_supports( $feature ) ) {
			require_once dirname( __FILE__ ) . '/features/' . $feature . '/' . $feature . '.php';
		}
	}
}

add_action( 'init', 'x5_load_features' );

/*
 * Add basic functionality required by WordPress.org
 */
if ( function_exists( add_theme_support( 'seo-title' ) ) ) {
	add_theme_support( 'seo-title' );
}

if ( function_exists( add_theme_support( 'threaded-comments' ) ) ) {
	add_theme_support( 'threaded-comments' );
}

if ( function_exists( add_theme_support( 'comments' ) ) ) {
	add_theme_support( 'comments' );
}

if ( function_exists( add_theme_support( 'automatic-feed-links' ) ) ) {
	add_theme_support( 'automatic-feed-links' );
}

if ( function_exists( add_theme_support( 'title-tag' ) ) ) {
	add_theme_support( 'title-tag' );
}

if ( function_exists( add_theme_support( 'menus' ) ) ) {
	add_theme_support( 'menus',
		array(
			'navigation-top_left' => __( 'Top Navigation Left Menu', 'x5' ),
			'navigation-top_right' => __( 'Top Navigation Right Menu', 'x5' ),
		)
	);
}


/*
 * Register menus
 */
function x5_register_menus() {

	register_nav_menus(
		array(
			'header-left-menu' => esc_html__( 'Header Left Menu', 'x5' ),
			'header-right-menu' => esc_html__( 'Header Right Menu', 'x5' ),
		)
	);
}
add_action( 'init', 'x5_register_menus' );


/*
* Add default sidebars
*/
function x5_register_sidebars() {

	register_sidebar(
			array(
				'id'            => 'primary',
				'name'          => __( 'Primary Sidebar', 'x5' ),
				'description'   => __( 'Main content sidebar.', 'x5' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
	);
	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'x5_register_sidebars' );


if ( ! isset( $content_width ) ) {
	$content_width = 1210;
}


/*
 * Add post thumbnails
 */
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 525, 340, true ); // true, so that the image is cropped to exact this size


/*
 * Add new image sizes
 */
if ( function_exists( 'add_image_size' ) ) {
	//add_image_size( 'home_story_about', 524, 295, true );
}


/*
 * Add image sizes to uploader
 */
function x5_add_image_sizes() {
	add_image_size( 'post_image', 712, 340, true );
}
add_action( 'init', 'x5_add_image_sizes' );

function x5_show_image_sizes( $sizes ) {
	$sizes['post_image'] = __( 'Post Image', 'x5' );
 	return $sizes;
}
add_filter( 'image_size_names_choose', 'x5_show_image_sizes' );


/*
 * Load optimization
 */
// Remove Emoji icons
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/*
 * Enqueue styles and scripts
 */
function x5_add_scripts() {

	// Header
	wp_enqueue_style( 'x5-style', get_stylesheet_uri() );

	wp_enqueue_style( 'x5-main', get_template_directory_uri() . '/css/main.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


	// Footer
	wp_enqueue_script( 'x5-plugins', get_template_directory_uri() . '/js/vendor.js', array( 'jquery' ), '29122018', true );

	wp_register_script( 'x5-js', get_template_directory_uri() . '/js/app.bundle.js', array( 'jquery', 'x5-plugins' ), '29122018', true );

	// Home page scripts - GoogleMaps and variables passed to main-head.js
	if ( is_page_template( 'page-home.php' ) &&
			 get_field( 'x5_home_intro_video_yt' ) ||
			 get_field( 'x5_home_intro_video_poster' ) ) {

		$x5_home_intro_video_yt_ID = get_field( 'x5_home_intro_video_yt' );

		$x5_video_values = array(
			'video_yt' => esc_js( $x5_home_intro_video_yt_ID ),
		);
		// Pass js values to app.bundle.js file
		wp_localize_script( 'x5-js', 'x5_video', $x5_video_values );
	}

	wp_enqueue_script( 'x5-js' );

}
add_action( 'wp_enqueue_scripts', 'x5_add_scripts' );


/**
 * Add defer attribute to loaded scripts
 */
function x5_add_scripts_defer_attribute( $tag, $handle ) {
	$scripts_to_defer = array( 'jquery',
															'x5-plugins',
															'x5-js' );

	foreach( $scripts_to_defer as $defer_script ) {
		if ( $defer_script === $handle ) {
			return str_replace( ' src', ' defer="defer" src', $tag );
		}
	}
	return $tag;
}
add_filter( 'script_loader_tag', 'x5_add_scripts_defer_attribute', 10, 2 );


/*
 * Add ACF Options pages
 */
if( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page();

	acf_add_options_sub_page( 'Header' );
}


/*
 * Add styles to editor
 */
function x5_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/css/main.css' );
}
add_action( 'admin_init', 'x5_add_editor_styles' );


/*
 * Add client defined styles to header
 */
function x5_add_customized_css() {

	?>

	<style>

		<?php

			get_template_part( 'inc/css/parts', 'general' );
			get_template_part( 'inc/css/page', 'home' );
			get_template_part( 'inc/css/page', 'contact' );
			get_template_part( 'inc/css/page', 'philosophy' );
			get_template_part( 'inc/css/sections', 'content' );

		?>

	</style>

	<?php
}
add_action( 'wp_head', 'x5_add_customized_css' );


/*
 * Add shortcodes
 */
// [list_short_width][/list_short_width]
function x5_add_shortcode_list_short_width( $attr, $content = null ) {
	return '<div class="short">'. $content .'</div>';
}
// [subtitle][/subtitle]
function x5_add_shortcode_subtitle( $attr, $content = null ) {
	return '<p class="subtitle">'. $content .'</p>';
}

/*
 * Register shortcodes
 */
function x5_register_shortcodes(){
	add_shortcode( 'list_short_width', 'x5_add_shortcode_list_short_width' );
	add_shortcode( 'subtitle', 'x5_add_shortcode_subtitle' );
}
add_action( 'init', 'x5_register_shortcodes');


/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';



/* Auto scroll to GravityForms confirmation message */
//add_filter( 'gform_confirmation_anchor', '__return_true' );