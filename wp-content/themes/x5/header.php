<?php
/**
 * X5: Header
 *
 * Contains dummy HTML to show the default structure of WordPress header file
 *
 * Remember to always include the wp_head() call before the ending </head> tag
 *
 * Make sure that you include the <!DOCTYPE html> in the same line as ?> closing tag
 * otherwise ajax might not work properly
 *
 * @package WordPress
 * @subpackage X5
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="initial-scale=1, minimum-scale=1, width=device-width">

  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <!-- optional -->
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!-- end of optional -->

  <script>
    document.createElement('picture');
  </script>

  <script>
    document.querySelector('html').classList.remove('no-js');
  </script>

  <?php

    // do not remove
    wp_head();
  ?>

</head>

<body
  <?php
    if ( is_page_template( 'page-home.php' ) ){
      body_class( 'page-home' );
    } elseif ( is_page_template( 'page-about.php' ) ){
      body_class( 'page-about' );
    } elseif ( is_page_template( 'page-contact.php' ) ){
      body_class( 'page-contact' );
    } elseif ( is_page_template( 'page-philosophy.php' ) ){
      body_class( 'page-philosophy' );
    } elseif ( is_page_template( 'page-custom.php' ) ){
      body_class( 'page-custom' );
    } elseif ( is_page_template( 'page-blog.php' ) ) {
      body_class( 'page-blog' );
    } elseif ( is_404()) {
      body_class( 'error404' );
    }
  ?>>

  <header class="c-header">

    <div class="container">

      <div class="row justify-content-start justify-content-lg-between">
        <div class="col-lg-12">

          <div class="c-nav js-nav">

            <div class="c-nav__btn js-nav__btn"></div>

            <h1 class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

            <!-- / Display menu -->
            <?php
              if ( has_nav_menu( 'header-left-menu' ) ):
                wp_nav_menu(
                  array(
                    'theme_location' => 'header-left-menu',
                    'container_class' => 'c-nav__menu js-nav__menu left',
                  )
                );
              endif;
            ?>

            <!-- / Display menu -->
            <?php
              if ( has_nav_menu( 'header-right-menu' ) ):
                wp_nav_menu(
                  array(
                    'theme_location' => 'header-right-menu',
                    'container_class' => 'c-nav__menu js-nav__menu right',
                  )
                );
              endif;
            ?>

          </div>
          <!-- / c-navigation -->

        </div>
        <!-- col -->

      </div>
      <!-- row -->

    </div>
    <!-- / container -->

  </header>
  <!-- / c-header -->