<?php
/**
* Template Name: Philosophy
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_philosophy_intro_desc' ) ):
	$x5_page_sections_rows_align_class = '';
	$x5_page_sections_text_align_class = '';

	if ( get_field( 'x5_philosophy_intro_align' ) == 'right' ):
		$x5_page_sections_rows_align_class = 'right-align';
		$x5_page_sections_text_align_class = 'justify-content-end';
	endif; ?>

	<section class="c-content c-content--inner <?php echo esc_attr( $x5_page_sections_rows_align_class ); ?>">

    <div class="container">

      <div class="row <?php echo esc_attr( $x5_page_sections_text_align_class ); ?>">
        <div class="col-lg-6 intro">

          <h2>PHILOSOPHY</h2>
          <p>We believe that real wealth – an income you do not outlive and a significant legacy to your heirs - is achievable for regular people.  We believe that achieving real wealth is not driven primarily by investment performance but by investor behavior – making the right decisions at the critical moment.</p>
          <p>We believe good investor behavior is primarily a function of making a lifetime financial plan and sticking with it, ignoring the fads and fears of the moment.</p>
          <p>We invest with a buy and hold, long term investment strategy formulated based on a comprehensive financial plan.  We do not attempt to time the highs and lows of the markets or individual investments.</p>

        </div>
        <!-- col -->
      </div>
      <!-- row -->

    </div>
    <!-- container -->

  </section>
  <!-- c-content -->

<?php endif; ?>


<?php get_footer();
