<?php

// Philosophy page
if ( is_page_template( 'page-philosophy.php' ) ):

	// Content section
	if ( get_field( 'x5_philosophy_intro_bg' ) ):
		$x5_philosophy_intro_bg = get_field( 'x5_philosophy_intro_bg' );

		?>
			.page-philosophy .c-content:after {
			  background: transparent url('<?php echo esc_url( $x5_philosophy_intro_bg['url'] ); ?>') center center no-repeat;
			  background-size: cover;
			}
		<?php
	endif;
	// end of content section

endif;
// end of Philosophy page