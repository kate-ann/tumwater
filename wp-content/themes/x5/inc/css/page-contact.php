<?php

// Contact page
if ( is_page_template( 'page-contact.php' ) ):

	// Intro section
	if ( get_field( 'x5_contact_intro_bg' ) ):
		$x5_contact_intro_bg = get_field( 'x5_contact_intro_bg' );
		?>
			.page-contact .c-content:after {
			  background: transparent url('<?php echo esc_url( $x5_contact_intro_bg['url'] ); ?>') center center no-repeat;
			  background-size: cover;
			}
		<?php
	endif;
	// end of Intro section

endif;
// end of Contact page