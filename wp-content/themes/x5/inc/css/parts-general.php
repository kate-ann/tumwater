<?php

// Header logo
if ( get_field( 'x5_header_logo', 'option' ) &&
	 	 get_field( 'x5_header_logow', 'option' ) &&
	 	 get_field( 'x5_header_logoh', 'option' ) ):
	// SVG image
	$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

	.c-logo {
		width: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logow', 'option' ) ); ?>;
		height: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logoh', 'option' ) ); ?>;
	  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
	  background-size: <?php echo wp_filter_nohtml_kses( get_field( 'x5_header_logow', 'option' ) ); ?> auto;
	}

	<?php
elseif ( get_field( 'x5_header_logo', 'option' ) ):
	// PNG/Jpeg image
	$x5_header_logo = get_field( 'x5_header_logo', 'option' ); ?>

	.c-logo {
		width: <?php echo wp_filter_nohtml_kses( $x5_header_logo['width'] ); ?>px;
		height: <?php echo wp_filter_nohtml_kses( $x5_header_logo['height'] ); ?>px;
	  background: transparent url("<?php echo esc_url( $x5_header_logo['url'] ); ?>") 0 0 no-repeat;
	}

	<?php

endif;
// end of General parts