<?php

// Home page
if ( is_page_template( 'page-home.php' ) ):
	if ( get_field( 'x5_home_intro_video_poster' ) ):
		$x5_home_intro_video_poster = get_field( 'x5_home_intro_video_poster' );
	var_dump($x5_home_intro_video_poster);
		?>
			.page-home .c-intro {
				background: transparent url('<?php echo esc_url( $x5_home_intro_video_poster['url'] ); ?>') 0 0 no-repeat;
				background-size: cover;
			}
		<?php
	endif;
endif;
// end of Home page