<?php

// About or Who pages
if ( is_page_template( 'page-about.php' ) ||
		 is_page_template( 'page-who.php' ) ):

	// Sections with content
	if ( have_rows( 'x5_page_sections_rows' ) ):
		$x5_page_sections_row_count = 1;

		while( have_rows( 'x5_page_sections_rows' ) ) : the_row();
			if ( get_sub_field( 'x5_page_sections_rows_bg' ) ):
				$x5_page_sections_rows_bg = get_sub_field( 'x5_page_sections_rows_bg' );

				?>
					.c-content.<?php echo esc_html( get_sub_field( 'x5_page_sections_rows_id' ) ); ?>:after {
					  background: transparent url('<?php echo esc_url( $x5_page_sections_rows_bg['url'] ); ?>') center center no-repeat;
					  background-size: cover;
					}
				<?php
			endif;
			$x5_page_sections_row_count ++;
		endwhile;
	endif;
	// end of section with content

endif;
// end of About and Who page