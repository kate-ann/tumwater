<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_home_intro_logo' ) ||
					 get_field( 'x5_home_intro_desc' ) ||
					 get_field( 'x5_home_intro_video_mp4' ) ||
					 get_field( 'x5_home_intro_video_webm' ) ||
					 get_field( 'x5_home_intro_video_ogg' ) ||
					 get_field( 'x5_home_intro_video_ogv' ) ): ?>

	<section class="c-intro">

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-12">

					<div class="row justify-content-center">

						<?php if ( get_field( 'x5_home_intro_logo' ) ):
							$x5_home_intro_logo = get_field( 'x5_home_intro_logo' ); ?>

							<div class="col-md-9 col-lg-3 logo">
								<img src="<?php echo esc_url( $x5_home_intro_logo['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_intro_logo['alt'] ); ?>" />
							</div>
							<!-- col -->
						<?php endif; ?>

						<?php if ( get_field( 'x5_home_intro_desc' ) ): ?>
							<div class="col-md-9 col-lg-9 col-xxl-7 intro">
								<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_desc' ) ) ); ?>
							</div>
							<!-- div -->
						<?php endif; ?>

					</div>
					<!-- row -->

				</div>
				<!-- col -->
			</div>
			<!-- row -->
		</div>
		<!-- container -->

		<?php if ( get_field( 'x5_home_intro_video_yt' ) ): ?>
			<div id="js-intro-player" class="c-video"></div>
		<?php endif; ?>
	</section>
	<!-- c-intro -->

<?php endif; ?>

<?php get_footer();
