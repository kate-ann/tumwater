<?php
/**
 * X5: 404 page
 *
 * Contains some dummy HTML with sample content
 * http://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
?>

<div class="content">
  <div class="o-container">
  	<p>No page was found. Perhaps checking one of these categories might help?</p>
  		<ul>
  			<?php wp_list_categories( 
  				array(
  					'orderby' => 'count',
  					'order' => 'DESC',
  					'show_count' => 1,
  					'title_li' => '',
  					'number' => 10
  				) 
  			); ?>
  		</ul>
  </div>
</div>

<?php get_footer();
