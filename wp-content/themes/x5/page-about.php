<?php
/**
* Template Name: About
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php get_template_part( 'partials/sections', 'content' ); ?>

<?php get_footer();