<?php
/**
 * X5: Footer
 *
 * Remember to always include the wp_footer() call before the </body> tag
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<?php
	// do not remove
	wp_footer();
?>

</body>
</html>
