<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_contact_intro_logo' ) ||
					 get_field( 'x5_contact_intro_left_desc' ) ||
					 get_field( 'x5_contact_intro_right_desc' ) ||
					 get_field( 'x5_contact_intro_social_btns' ) ): ?>

	<section id="section-01" class="c-content c-content--inner right-align">

  	<div class="container">

  		<div class="row justify-content-end">
  			<div class="col-lg-6 intro">

					<?php if ( get_field( 'x5_contact_intro_logo' ) ):
						$x5_contact_intro_logo = get_field( 'x5_contact_intro_logo' );
						?>

						<span class="logo">
							<img src="<?php echo esc_url( $x5_contact_intro_logo['url'] ); ?>" alt="<?php echo esc_attr( $x5_contact_intro_logo['alt'] ); ?>" />
						</span>

					<?php endif; ?>

					<?php if ( get_field( 'x5_contact_intro_left_desc' ) ||
										 get_field( 'x5_contact_intro_right_desc' ) ): ?>

						<div class="addr">

							<?php if ( get_field( 'x5_contact_intro_left_desc' ) ): ?>
								<div class="column"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_contact_intro_left_desc' ) ) ); ?></div>
							<?php endif; ?>

	            <?php if ( get_field( 'x5_contact_intro_right_desc' ) ): ?>
	            	<div class="column"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_contact_intro_right_desc' ) ) ); ?></div>
	            <?php endif; ?>

	          </div>
	          <!-- addr -->

					<?php endif; ?>

					<?php get_template_part( 'partials/social', 'buttons' ); ?>

  			</div>
  			<!-- col -->
  		</div>
  		<!-- row -->

  	</div>
  	<!-- container -->

  </section>
  <!-- c-content -->

<?php endif; ?>


<?php get_footer();
